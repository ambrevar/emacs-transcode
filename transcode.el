;;; transcode.el --- Transcode audio/video with Emacs -*- lexical-binding: t -*-

;; Copyright (C) 2019, 2020  Free Software Foundation, Inc.

;; Author: Pierre Neidhardt <mail@ambrevar.xyz>
;; Maintainer: Pierre Neidhardt <mail@ambrevar.xyz>
;; URL: https://gitlab.com/Ambrevar/emacs-transcode
;; Version: 0.0.1
;; Package-Requires: ((emacs "26.1"))
;; Keywords: audio, video, tools

;; This file is not part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; Run `transcode-profile'.

;; This is an early version.

;; TODO: Implement crop option.
;; TODO: Implement audio/vido/sub delay option.
;; TODO: Profile to rearrange streams without encoding.  If video is left out,
;; output to the right audio format.
;; TODO: Implement option to set stream metadata, like title.  To remove title:
;; -metadata:s:$i title= "
;; TODO: Implement option to insert stream from foreign files.
;; TODO: Implement option to concatenate streams from different files.

;;; Code:
(require 'cl-lib)
(require 'json)
(require 'subr-x)

(require 'eshell)

(defvar transcode-command "ffmpeg")
(defvar transcode-ffprobe-command "ffprobe")
(defvar transcode-args '())
(defvar transcode-buffer-name "*transcode*")
(defvar transcode-profiles '()
  "A profile is a function returning a function that returns a list of commands.")
(defvar transcode-extensions
  '("avi"
    "flv"
    "mkv"
    "mp4"
    "mpeg"
    "mpg"
    "ogm"
    "webm"
    "wmv")
  "List of extensions to match for `transcode-all-profile'.")

(defvar transcode-ask-for-sample t
  "When non-nil, prompt if a sample should be generated.")

(defun transcode-file-information (file)
  (json-read-from-string
   (with-output-to-string
     (with-current-buffer standard-output
       (apply 'process-file transcode-ffprobe-command nil t nil
              (list "-v"
                    "quiet"
                    "-print_format"
                    "json"
                    "-show_format"
                    "-show_streams"
                    file))))))

(defun transcode-stream-display (stream)
  "Format stream into a human-readable line.
Commas (',') are replaced by semicolons so that these lines can
be used in `completing-read-multiple'."
  (replace-regexp-in-string
   "," ";"
   (mapconcat #'identity
              (append
               (mapcar (lambda (key)
                         (format "%s" (alist-get key stream)))
                       '(index codec_type codec_name))
               (list (format "(%s)"
                             (mapconcat (lambda (pair)
                                          (format "%s: %s" (first pair) (rest pair)))
                                        (alist-get 'tags stream)
                                        ", "))))
              " ")))

(defun transcode-args-sample (&optional value)
  (if transcode-ask-for-sample
      (let ((duration
             (or value (completing-read "Sample duration (s): "
                                        (mapcar #'number-to-string '(0 60 300 600))
                                        nil nil nil nil "0"))))
        (if (string= duration "0")
            '()
          ;; Start at one minute, since opening is usually a still picture.
          `("-ss" "60" "-t" ,duration)))
    '()))

(defun transcode-args-video-codec (&optional value)
  (list "-c:v" (or value
                   (completing-read "Video codec: "
                                    '("copy"
                                      "libx264")
                                    nil nil nil nil "libx264"))))

(defun transcode-args-video-preset (&optional value)
  (list "-preset"
        (or value
            (completing-read "Video preset: "
                             '("ultrafast"
                               "superfast"
                               "veryfast"
                               "faster"
                               "fast"
                               "medium"
                               "slow"
                               "slower"
                               "veryslow"
                               "placebo")
                             nil nil nil nil "slower"))))

(defun transcode-args-video-quality (&optional value)
  (list "-crf"
        (or
         value
         (completing-read "Video quality (higher means lower quality): "
                          '("18"        ; near perfection
                            "20"        ; good compromise
                            ;; really good compression, while a bit more blurry than the original
                            "22")
                          nil nil nil nil "20"))))

(defun transcode-args-video-bitrate (&optional value)
  (list "-b:v"
        (concat (or value
                    (completing-read
                     "Video bitrate (kb/s): "
                     (mapcar #'number-to-string (number-sequence 1000 10000 1000))
                     nil nil nil nil "5000"))
                "k")))

(defun transcode-args-video-tune (&optional value)
  (let ((result
         (or value (completing-read "Tuning (empty for none): "
                                    '(;; See `x264 --fullhelp'.
                                      "film"
                                      "animation"
                                      "grain")))))
    (if (string-empty-p result)
        '()
      `("-tune" ,result))))

(defun transcode-args-audio-codec (&optional value)
  (list "-c:a" (or value (completing-read "Audio codec: "
                                          '("libvorbis"
                                            "copy")
                                          nil nil nil nil "libvorbis"))))

(defun transcode-args-audio-bitrate (&optional value)
  (let ((result (or value (completing-read "Audio bitrate, empty means copy (kb/s): "
                                           '("128"
                                             "160"
                                             "192")
                                           nil nil nil nil "160"))))
    (if (string-empty-p result)
        '()
      `("-b:a" ,(concat result "k")))))

(defun transcode-args-audio-mixdown (&optional value)
  "This greatly reduce file size and avoid any confusion for
playback, which is often the case when converting DTS to any
other format.  (DTS has embedded channel description which is not
always available in other formats.)"
  (let ((result (or value (yes-or-no-p "Mixdown audio to 2 channels?"))))
    (if result
        '("-ac" "2")
      '())))

(defun transcode-args-subtitle-codec ()
  (list "-c:s" "copy"))

(defun transcode-args-data-discard (&optional value)
  (let ((result (or value (yes-or-no-p "Discard extraneous data?"))))
    (if result
        '("-dn")
      '())))

(defun transcode-args-stream-selection (input &optional value)
  "Result is in the form `(\"0:1\" \"0:3\"...)', following the `-map' command line
option of FFmpeg.
If VALUE is nil and INPUT is a file, query the user for the INPUT streams to include.
If INPUT is also nil, fallback to default.
The result `(\"0\")' means \"all streams\"."
  (apply
   #'append
   (mapcar (lambda (arg)
             (list "-map" arg))
           (or value
               (when input
                 (mapcar
                  (lambda (stream)
                    (if (string= stream "0")
                        "0"
                      ;; First token is the stream index.  This is a bit of a
                      ;; kludge, could be improved.
                      (concat "0:" (first (split-string stream)))))
                  (completing-read-multiple
                   "Ordered list of streams to include (empty for all): "
                   (mapcar #'transcode-stream-display
                           (alist-get 'streams (transcode-file-information input)))
                   nil nil nil nil "0")))
               '("0")))))

(defun transcode-audio-streams (input)
  "Return a list of audio streams.
Each stream is an alist.
Codec name is accessible with the 'codec_name key."
  (append
   (cl-delete-if
    (lambda (s)
      (not (string= (alist-get 'codec_type s) "audio")))
    (alist-get 'streams
               (transcode-file-information input)))
   nil))

(defun transcode-args-output (output)
  (let ((output (if (string= (file-name-extension output) "mp4")
                    output
                  (concat (file-name-sans-extension output) ".mkv"))))
    (if (file-exists-p output)
        ;; Bind `temporary-file-directory' so that `make-temp-file' outputs in the
        ;; same directory.
        (let ((temporary-file-directory (file-name-directory output)))
          (make-temp-file (concat (file-name-base
                                   (file-name-sans-extension output))
                                  "-")
                          nil
                          (concat "." (file-name-extension output))))
      (make-directory (file-name-directory output) :parents)
      output)))

(cl-defun transcode-input-output (input output args
                                        &key pre-input-args force-output-path-p)
  "Return a command to be processed by `transcode-make-command'."
  (unless force-output-path-p
    (setq output (transcode-args-output output)))
  (append
   (list transcode-command "-nostdin"
         ;; "-v" "warning" ; Change log level?
         ;; TODO: Allow overwrite?
         ;; "-y"
         )
   pre-input-args
   (list "-i" input)
   args
   (list output)))

(cl-defun transcode-input-output-2pass (input output args &key pre-input-args)
  "Return a list of commands to be processed by `transcode-make-command'."
  (list (transcode-input-output input "/dev/null"
                                (append (list "-pass" "1")
                                        args
                                        (list "-f" "matroska"))
                                ;; First pass needs to ignore existence of /dev/null with -y.
                                :pre-input-args (cons "-y" pre-input-args)
                                :force-output-path-p t)
        (transcode-input-output input output
                                (append (list "-pass" "2")
                                        args
                                        pre-input-args))
        (list "rm" "-v" "ffmpeg2pass-0.log" "ffmpeg2pass-0.log.mbtree")))

(defun transcode-custom ()
  (let* ((2pass? nil)
         (params (append
                  (transcode-args-video-codec)
                  (transcode-args-video-preset)
                  (if (yes-or-no-p "Two-pass?")
                      (progn
                        (setq 2pass? t)
                        (transcode-args-video-bitrate))
                    (transcode-args-video-quality))
                  (transcode-args-video-tune)
                  (transcode-args-audio-codec)
                  (transcode-args-audio-bitrate)
                  (transcode-args-audio-mixdown)
                  (transcode-args-subtitle-codec)
                  (transcode-args-data-discard)))
         (sample-params (transcode-args-sample)))
    (if 2pass?
        (lambda (input output)
          (transcode-input-output-2pass input output
                                        (append params
                                                (transcode-args-stream-selection input))
                                        :pre-input-args sample-params))
      (lambda (input output)
        (list (transcode-input-output input output
                                      (append params
                                              (transcode-args-stream-selection input)
                                              sample-params)))))))
(add-to-list 'transcode-profiles #'transcode-custom)

(defun transcode-generic ()
  "Slow, high quality, with Vorbis conversion and down-mixing.
Video has no tuning."
  (let ((params (append
                 (transcode-args-video-codec "libx264")
                 (transcode-args-video-preset "slower")
                 (transcode-args-video-quality "18")
                 (transcode-args-audio-codec "libvorbis")
                 (transcode-args-audio-bitrate "160")
                 (transcode-args-audio-mixdown t)
                 (transcode-args-subtitle-codec)
                 (transcode-args-data-discard t)
                 (transcode-args-stream-selection nil '("0"))
                 (transcode-args-sample))))
    (lambda (input output)
      (list (transcode-input-output input output params)))))
(add-to-list 'transcode-profiles #'transcode-generic)

(defun transcode-select-streams ()
  "Only keep selected streams.
This profile cannot be used to batch-process directories."
  (let ((params (append
                 (transcode-args-video-codec "copy")
                 (transcode-args-audio-codec "copy")
                 (transcode-args-subtitle-codec)
                 (transcode-args-data-discard))))
    (lambda (input output)
      (list (transcode-input-output input output
                                    (append params
                                            (transcode-args-stream-selection input)))))))
(add-to-list 'transcode-profiles #'transcode-select-streams)

(defun transcode-audio-only ()
  "Only keep audio stream."
  (let ((params (append
                 '("-vn")
                 (transcode-args-audio-codec "copy")
                 '("-sn")
                 '("-dn"))))
    (lambda (input output)
      (when (member (file-name-extension output) transcode-extensions)
        (setq output (concat (file-name-sans-extension output) "."
                             (alist-get 'codec_name
                                        (first (transcode-audio-streams input))))))
      (list (transcode-input-output input output
                                    params
                                    :force-output-path-p t)))))
(add-to-list 'transcode-profiles #'transcode-audio-only)

(defun transcode-2pass ()
  "Same as `transcode-generic' but with two-pass encoding and asks for video bitrate.
Video has \"film\" tuning."
  (let ((params (append (transcode-args-video-codec "libx264")
                        (transcode-args-video-preset "slower")
                        (transcode-args-video-bitrate)
                        (transcode-args-video-tune "film")
                        (transcode-args-audio-codec "libvorbis")
                        (transcode-args-audio-bitrate "160")
                        (transcode-args-audio-mixdown t)
                        (transcode-args-subtitle-codec)
                        (transcode-args-data-discard t)
                        (transcode-args-stream-selection nil '("0"))))
        (sample-params (transcode-args-sample)))
    (lambda (input output)
      (transcode-input-output-2pass input output params
                                    :pre-input-args sample-params))))
(add-to-list 'transcode-profiles #'transcode-2pass)

(defvar transcode-prefer-eshell (and (boundp 'helm-ff-preferred-shell-mode)
                                     (eq helm-ff-preferred-shell-mode 'eshell-mode)))

(defun transcode-run (commands)
  "COMMANDS is a list of strings."
  (if transcode-prefer-eshell
      (transcode-run-eshell commands)
    (transcode-run-shell commands)))

(defun transcode-shell-alive-p (mode)     ; Copied from `helm-ff-shell-alive-p'.
  "Returns non nil when a process is running inside `shell-mode' buffer."
  (cl-ecase mode
    (shell-mode
     (save-excursion
       (comint-goto-process-mark)
       (or (null comint-last-prompt)
           (not (eql (point)
                     (marker-position (cdr comint-last-prompt)))))))
    (eshell-mode
     (get-buffer-process (current-buffer)))))

(defun transcode-run-shell (commands)
  "COMMANDS is a list of strings."
  (shell (get-buffer-create transcode-buffer-name))
  (let ((proc (get-buffer-process (current-buffer))))
    (when proc (accept-process-output proc 0.1)))
  (unless (transcode-shell-alive-p major-mode)
    (goto-char (point-max))
    (comint-delete-input)
    (insert (mapconcat #'identity commands " ; \\\n"))
    (comint-send-input)))

(defun transcode-run-eshell (commands)
  "COMMANDS is a list of strings."
  (let ((eshell-buffer-name transcode-buffer-name))
    (eshell)
    (when (eshell-interactive-process)
      (eshell t))
    (eshell-interrupt-process)
    (insert (mapconcat #'identity commands " ; \\\n"))
    (eshell-send-input)))

(defun transcode-compile-profile (profile)
  "See `transcode-make-command'."
  (funcall (intern profile)))

(defun transcode-make-command (input output compiled-profile)
  "Return a string command that can be run in a shell.
COMPILED-PROFILE is a function that takes INPUT and OUTPUT arguments and returns
lists of strings, each list being a command."
  (let* ((commands (funcall compiled-profile input output)))
    (string-join (mapcar (lambda (command)
                           (mapconcat
                            (if transcode-prefer-eshell
                                #'eshell-quote-argument
                              #'comint-quote-filename)
                            command
                            " "))
                         commands)
                 " && ")))

(defun transcode (input output &optional profile)
  "If PROFILE is not specified, prompt user for a value in `transcode-profiles'"
  (interactive "fInput: \nFOutput: ")
  (setq profile (or profile
                    (completing-read "Profile: " transcode-profiles)))
  (transcode-run (list (transcode-make-command input output (transcode-compile-profile profile)))))

(defun transcode-directory (input-dir output-dir &optional profile)
  "Like `transcode' for process all files matching `transcode-extensions' in INPUT-DIR.
Compiled profiles are made non-interactive to avoid prompting the user on each
file.."
  (interactive "DInput directory: \nDOutput directory: ")
  (setq profile (or profile
                    (completing-read "Profile: " transcode-profiles)))
  (let* ((compiled-profile (transcode-compile-profile profile))
         (files (cl-loop for file in (directory-files-recursively input-dir ".")
                         when (string-match (concat (regexp-opt transcode-extensions) "$")
                                            (downcase file))
                         collect file))
         (silent-completing-read (lambda (_prompt _collection &optional
                                                  _predicate _require-match _initial-input _hist
                                                  def
                                                  _inherit-input-method)
                                   (if (listp def)
                                       (first def)
                                     def)))
         (silent-completing-read-multiple (lambda (_prompt _collection &optional
                                                  _predicate _require-match _initial-input _hist
                                                  def
                                                  _inherit-input-method)
                                            (list def)))
         (commands (cl-loop for input in files
                            for output = (expand-file-name (file-name-nondirectory input) output-dir)
                            collect (cl-letf (((symbol-function 'yes-or-no-p)
                                               (lambda (_) t))
                                              ((symbol-function 'completing-read) silent-completing-read)
                                              ((symbol-function 'completing-read-multiple) silent-completing-read-multiple))
                                      (transcode-make-command input output compiled-profile)))))
    (transcode-run commands)))

(provide 'transcode)
;;; transcode.el ends here
